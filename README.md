# CTF Write up cannon

This repo is to store a compilation of ctf writeups that demonstrate effective use of different techniques for playing CTF. Most (if not all) of the writeups here are not the work of Cyber@UC, and are archived so that they may be read even after the original sources have gone down.

# Write Up List

## PWN
Challenges where you have to discover a vulnerability and write an exploit for that vulnerability, usually to pop a shell on the server. (separate from [Jail](Write-Up-list) and sandbox as those are often their own categories

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |
| ROP: bypass NX, ASLR, PIE and Canary | MANUEL LÓPEZ PÉREZ |  | [original](https://ironhackers.es/en/tutoriales/pwn-rop-bypass-nx-aslr-pie-y-canary/) | rop, aslr bypass, canary bypass |
| N/A | m_101 | N/A | [original](http://binholic.blogspot.com/2017/05/notes-on-abusing-exit-handlers.html) | abusing exit handlers |
| N/A | ? | ? | [original](https://ctf-wiki.github.io/ctf-wiki/pwn/linux/fmtstr/fmtstr_example/) | format string vulnerabilities |
| LazyHouse | ? | HITCON CTF Qualifiers 2019 | [original](https://faraz.faith/2019-10-24-hitconctf-lazyhouse-balsn-exploit-analysis/) | malloc, calloc, heap, rop |


## REV
Challenges where you have to reverse engineer a binary, unpack malware-ish binaries, etc. to get a flag (often stored in the binary)

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |
| SPRINT | Zion L. Basque | Google CTF 2020 | [original](https://mahaloz.re/ctf/google-quals-2020-sprint/) | printf format strings, printf oriented programming, intermediate representation |


## CRYPTO
Challenges for people that like math an guessing

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |
| PLZ | FILL | ME | IN | I |
| DON'T | KNOW | HOW | TO | CRYPTO &#x1f62d; |

## OSINT
Challenges that are essentially easter egg hunts around the internet. Given a certain set of information about someone like their name and an interest. Find any further information you can that will lead you to a flag somewhere along the internet.

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |
| Identity Fraud | Cyb3rDoctor | FWord(2020) | [original pdf](./osint_archive/Identity_Fraud_Solution.pdf) | Image lookup, Internet Archives, Social Media |
| G is Good | Cyb3rDoctor | FWord(2020) | [original pdf](./osint_archive/G_is_Good_Solution.pdf) | Google Account Enumeration |

## Jail
Break out of arbitrary limits on which characters / commands can be used in a shell or given environment.

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |
| minbashmaxfun | Ori Kadosh | 34c3 CTF 2017 | [original](https://medium.com/@orik_/34c3-ctf-minbashmaxfun-writeup-4470b596df60) | bash |
| \_\_nightmares__ | Martin Heistermann | Plaid CTF 2014 | [original](https://blog.mheistermann.de/2014/04/14/plaidctf-2014-nightmares-pwnables-375-writeup/) | Python2, Linux /proc/ |
| PyJail | Wannes Rombouts | Plaid CTF 2013 | [original](http://wapiflapi.github.io/2013/04/22/plaidctf-pyjail-story-of-pythons-escape.html) | Python2 |


## Sandbox
An advanced subset of PWN, these challenges require you to write an exploit for a process that restricts which syscalls you can use,  which permissions are available for the process (or memory page). Solutions for this category can sometimes take the entire duration of a CTF, and tend to be more complex than regular pwn.

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |
| teleport | Nguyen Hoang Trung | Google CTF 2020 | [original](https://trungnguyen1909.github.io/blog/post/GGCTF20/) | Full-Chain Exploit, Chromium |
| Writeonly | Cyb0rG from teambi0s | Google CTF 2020 | [original](https://blog.bi0s.in/2020/08/24/Pwn/GCTF20-Writeonly/) | side effects of fork() |


## Shellcoding
Another subset of pwn, this category skips the vulnerability discovery stage and instead imposes arbitrary restrictions on the format of your shellcode or sometimes the maximum size of your shellcode. An example would be shell code that must be valid lyrics to a song, or must be valid organic chemical names. See [This presentation from DEFCON](https://www.youtube.com/watch?v=qHj1kquKNk0) for a high level overview.

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |



# Misc
Challenges that don't always fully fit into another category end up here, but more often than not someone just didn't want to make jails or code golf its own category.

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |


# Code Golf
The time honored tradition of using as few characters as you can to perform a an arbitrary task specified by the CTF hosts. Either judged per character or by the file size of a compiled binary.

| Challenge Name | Write-up Author | CTF (if applicable) | Link | Techniques |
| ------ | ------ | ------ | ------ | ------ |
| golf.so | jkrupp of saarsec | PlaidCTF 2020 | [original](https://saarsec.rocks/2020/05/14/golf.so.html) | Creative z3 solution, Elf structure |



If you wish to add a write-up, please include the following:
- Author
- Category
- A link to the original write up (If the write-up is on a website)
- A clone of the web page (or at the very lease a copy of the text and images on the page) in a 7z/tar file (Preferred, not necessary)
- Techniques used/demonstrated
- Tools used (optional)
- A copy of any binaries/scripts/other files provided with the challenge. If a challenge black box, include what you can. (Optional)
