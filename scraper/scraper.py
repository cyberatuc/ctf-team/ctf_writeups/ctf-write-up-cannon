#!/usr/bin/python3
import re
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin
import os
import logging

l = logging.getLogger(__name__)
l.addHandler(logging.StreamHandler())
l.setLevel(logging.WARNING)


def get_retry(*args, session=None, max_retries=3, **kwargs):
    s = session if session is not None else requests.session()
    for retry_no in range(max_retries):
        try:
            r = s.get(*args, **kwargs)
        except Exception as err:
            l.warning('request failed %s', str(err.args))
            if retry_no == max_retries - 1:
                raise err

            # these exceptions prevent r from being created
            r = requests.Response
            r.status_code = 0

        if r.status_code == 200:
            break

    return r


def get_img_links(soup, response):
    """Returns an actual link to images and the original relative link from the page"""
    imgs = soup.find_all('img')
    img_srcs = [img['src'] for img in imgs if img.has_attr('src') is True]
    links = [(urljoin(response.url, src), src) if not src.startswith('http') else (src, src) for src in img_srcs]
    return list(set(links))


def get_stylesheet_links(soup, response):
    """Returns an actual link to stylesheets and the original relative link from the page"""
    links = soup.find_all('link')
    link_srcs = [link['href'] for link in links if link.has_attr('rel') is True and link['rel'] == ['stylesheet']]
    links = [(urljoin(response.url, src), src) if not src.startswith('http') else (src, src) for src in link_srcs]
    return list(set(links))


def get_usable_filename_from_url(url):
    """get a usable filename from a url. Pretty bad form but deals with sites like
    giphy that name all files the exact same thing"""
    cleaned_url = url[url.index(':') + 1:] if ':' in url else url
    cleaned_url = cleaned_url.strip('/').replace('/', '_')
    return cleaned_url


def download_media(url, destination, session=None):
    """url: url to download from
    destination: file path to save to"""
    s = session if session is not None else requests.session()
    r = s.get(url, stream=True)
    with open(destination, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)


def copy_page(url, destination_dir):
    s = requests.session()
    r = s.get(url)
    if r.status_code != 200:
        logging.warning('url %s responded with non 200 status code: %d', url, r.status_code)
        return
    base_page_filename = 'index.html'
    base_page_path = os.path.join(destination_dir, base_page_filename)
    # write to disk first in case downloading images goes poorly
    with open(base_page_path, 'wb') as f:
        f.write(r.content)
    l.info('%s Downloaded and saved', base_page_filename)

    l.info('Beginning to download additional resources')
    soup = BeautifulSoup(r.content, 'lxml')

    img_links = get_img_links(soup, r)
    stylesheet_links = get_stylesheet_links(soup, r)
    all_additional_links = img_links + stylesheet_links

    updated_page = r.content

    for link, rel_link in all_additional_links:
        new_filename = get_usable_filename_from_url(link)
        dest = os.path.join(destination_dir, new_filename)
        try:
            l.info('Downloading %s', link)
            download_media(link, dest, session=s)
        except Exception as err:
            l.debug('Issue downloading %s', err.args)
            l.warning('Issue downloading %s. This file will be skipped', link)
            continue

        replacement_path = os.path.join('.', new_filename)

        updated_page = re.sub(re.escape(rel_link.encode()), replacement_path.encode(), updated_page)

    l.info('Updating %s', base_page_path)
    with open(base_page_path, 'wb') as f:
        f.write(updated_page)
    l.info('%s updated', base_page_path)


def main(urls, destination_dir):

    destination_dir = os.path.abspath(destination_dir)
    # setup destination
    if not os.path.isdir(destination_dir):
        os.mkdir(destination_dir)

    for url in urls:
        copy_page(url, destination_dir)


if __name__ == '__main__':
    import argparse
    description = """A basic scraper to make archived copies of websites.
    This scraper only downloads html, media files, and css"""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-d', '--destination', default=os.getcwd(),
                        help='Destination directory. Defaults to current directory')
    parser.add_argument('urls', nargs=argparse.REMAINDER, default=[],
                        help='Urls to scrape')
    args = parser.parse_args()
    main(args.urls, args.destination)
